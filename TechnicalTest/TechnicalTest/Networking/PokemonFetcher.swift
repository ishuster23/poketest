//
//  PokemonFetcher.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/28/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//

import UIKit
import CoreData

class PokemonFetcher: NSObject {
  
  var managedObjectContext: NSManagedObjectContext!
  
  var requestedPokemon = [Pokemon]()
  
  override init() {
    super.init()
    managedObjectContext = DataAccessor.sharedInstance.managedObjectContext
  }
  
  func requestPokemons(completion: (finished: Bool) -> Void) {
    print("Downloading pokemons...")
    let serviceGroup = dispatch_group_create()
    
    for i in 1...10 {
      dispatch_group_enter(serviceGroup)
      fetchPokemon("\(i)").then { (pokemon) in
        self._addPokemon(pokemon)
        dispatch_group_leave(serviceGroup)
        }.onError { (error) in
          print(error)
          dispatch_group_leave(serviceGroup)
      }
    }
    
    dispatch_group_notify(serviceGroup, dispatch_get_main_queue()) {
      print("Finished downloading pokemons...")
      completion(finished: true)
    }
    
  }
  
  func _addPokemon(pokemon: PKMPokemon) {
    DataAccessor.sharedInstance.addPokemon(pokemon)
  }
}
