//
//  Pokemon+CoreDataProperties.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/28/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Pokemon {

    @NSManaged var height: NSNumber
    @NSManaged var identifier: NSNumber
    @NSManaged var imageURL: String
    @NSManaged var name: String
    @NSManaged var weight: NSNumber
    @NSManaged var stats: NSSet?

}
