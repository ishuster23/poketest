//
//  DataAccessor.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/28/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//

import UIKit
import CoreData

class DataAccessor: NSObject {
  
  static let sharedInstance = DataAccessor()
  
  var managedObjectContext: NSManagedObjectContext!
  
  func addPokemon(pokemon:PKMPokemon) {
    
    guard let pokemonEntity = NSEntityDescription.entityForName("Pokemon", inManagedObjectContext: managedObjectContext), statEntity = NSEntityDescription.entityForName("Stat", inManagedObjectContext: managedObjectContext) else {
      fatalError("Could not find entity descriptions!")
    }
    
    let _pokemon = Pokemon(entity: pokemonEntity, insertIntoManagedObjectContext: managedObjectContext)
    let unspecified = "Not set - try again later"
    _pokemon.name = pokemon.name ?? unspecified
    _pokemon.height = pokemon.height ?? 0
    _pokemon.identifier = pokemon.id ?? 0
    _pokemon.weight = pokemon.weight ?? 0
    _pokemon.imageURL = pokemon.sprites?.frontDefault ?? unspecified
    
    
    guard let stats = pokemon.stats else {
      print("no stats for this pokemon")
      return
    }
    
    let statsSet = NSMutableSet()
    
    for stat in stats {
      let _stat = Stat(entity: statEntity, insertIntoManagedObjectContext: managedObjectContext)
      _stat.name = stat.name ?? unspecified
      _stat.effort = 0
      _stat.baseStat = 0
      _stat.url = unspecified
      statsSet.addObject(_stat)
    }
    
    _pokemon.stats = NSSet(set: statsSet)
  }
  
  
  func saveContext() {
    print("Trying to save context")
    do {
      try managedObjectContext.save()
      print("contxt saved")
    } catch let error as NSError {
      print("Could not save data", error.localizedDescription)
    }
  }
  
  
  func deleteInformation() {
    let pokemons = currentPokemons()
    for pokemon in pokemons {
      managedObjectContext .deleteObject(pokemon)
    }
    saveContext()
  }
  
  
  func currentPokemons() -> [Pokemon] {
    let fetchRequest = NSFetchRequest(entityName: "Pokemon")
    do {
      if let results = try managedObjectContext.executeFetchRequest(fetchRequest) as? [Pokemon] {
        return results
      }
    } catch {
      fatalError("There was an error fetch the list of devices")
    }
    return [Pokemon]()
  }
}
