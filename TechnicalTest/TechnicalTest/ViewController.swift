//
//  ViewController.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/27/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//

import UIKit

class ViewController: GeneralViewController {
  
  
  // MARK: - IBOutlets
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var loginButton: UIButton!
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  // MARK: - Life view cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    AddKeyboardObservers()
    navigationController?.navigationBar.hidden = true  
  }
  
  
  // MARK: - Alerts
  func showIncorrectUserAndPassAlert() {
    let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
    simpleAlertControllerWithAction(okAction, title: "Something is wrong!", message: "Type your username and password")
  }
  
  // MARK: - Actions
  @IBAction func logInTouchUpInside(sender: AnyObject) {
    
    guard let username = usernameTextField.text, password = passwordTextField.text else {
      print("something wrong the user and pass text fields")
      return
    }
    
    if username.isEmpty || password.isEmpty {
      showIncorrectUserAndPassAlert()
      return
    }
    
    let identifier = String(HomeViewController)
    if let viewController = storyboard?.instantiateViewControllerWithIdentifier(identifier) {
      navigationController?.pushViewController(viewController, animated: true)
    }
  }
  
  
  // MARK: - Keyboard
  
  func AddKeyboardObservers() {
    print(#function)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
  }
  
  
  func keyboardWillHide(note: NSNotification) {
    let contentInsets = UIEdgeInsetsZero
    scrollView.contentInset = contentInsets
    scrollView.scrollIndicatorInsets = contentInsets
    self.view.layoutIfNeeded()
  }
  
  
  func keyboardWillShow(note: NSNotification) {
    if let userInfo = note.userInfo {
      if let keyboardSize =  (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() {
        let edgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.size.height/2.0, right: 0.0)
        scrollView.contentInset = edgeInsets
        scrollView.scrollIndicatorInsets = edgeInsets
        self.view.layoutIfNeeded()
      }
    }
  }
  
  
}

// MARK: - UIText field delegate

extension ViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField.returnKeyType == UIReturnKeyType.Next {
      passwordTextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
      logInTouchUpInside(loginButton)
    }
    
    return true
  }
  
}

