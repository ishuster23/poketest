//
//  UIViewController+Utilities.h
//  Credilikeme
//
//  Created by Alejandro Cárdenas on 9/3/15.
//  Copyright (c) 2015. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (NibLoading)

+ (id)loadViewControllerFromNib;

@end

@interface UIViewController (LoadingViews)

- (void)showLoadingView;
- (void)showLoadingViewWithTransparentBackground;
- (void)showLoadingViewWithMessage:(NSString *)message;
- (void)showLoadingViewWithMessage:(NSString *)message transparentBackground:(BOOL)transparentBackground;

- (void)removeLoadingView;
- (void)removeLoadingViewWithCompletion:(void (^)(void))completion;

- (BOOL)isShowingWaitingView;

@end

@interface UIViewController (AddChildViewControler)

// (06/06/16 - Aleks C. Barragan)
- (void)displayContentController:(UIViewController *)content;
- (void)hideContentController:(UIViewController *)content;

- (void)simpleAlertControllerWithAction:(UIAlertAction *)action title:(NSString *)title message:(NSString *)message;

- (void)alertControllerWithActions:(NSArray *)actions title:(NSString *)title message:(NSString *)message;


@end
