//
//  UIViewController+Utilities.m
//  Credilikeme
//
//  Created by Alejandro Cárdenas on 9/3/15.
//  Copyright (c) 2015. All rights reserved.
//

#import "UIViewController+Utilities.h"

#define kLoadingViewTag 10000
#define kActivityIndicatorViewTag 11000

#define isiOS8 [UIDevice currentDevice].systemVersion.floatValue >= 8.0

@implementation UIViewController (LoadingViews)

- (void)showLoadingView {
    [self showLoadingViewWithMessage:nil transparentBackground:NO];
}

- (void)showLoadingViewWithTransparentBackground {
    [self showLoadingViewWithMessage:nil transparentBackground:YES];
}

- (void)showLoadingViewWithMessage:(NSString *)message {
    [self showLoadingViewWithMessage:message transparentBackground:NO];
}

- (void)showLoadingViewWithMessage:(NSString *)message transparentBackground:(BOOL)transparentBackground {
    BOOL isLandscape = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
    CGFloat width = isiOS8 ? [UIScreen mainScreen].bounds.size.width : isLandscape ? [UIScreen mainScreen].bounds.size.height : [UIScreen mainScreen].bounds.size.width;
    CGFloat height = isiOS8 ? [UIScreen mainScreen].bounds.size.height : isLandscape ? [UIScreen mainScreen].bounds.size.width : [UIScreen mainScreen].bounds.size.height;
    height -= self.navigationController.navigationBar.frame.size.height;// + [UIApplication sharedApplication].statusBarFrame.size.height;
    CGRect frame = CGRectMake(0, 0, width, height);
    UIView *view = [[UIView alloc] initWithFrame:frame];
    [view setBackgroundColor:transparentBackground ? [UIColor clearColor] : [UIColor colorWithWhite:0.0 alpha:0.5]];
    view.tag = kLoadingViewTag;
    view.alpha = 0.0;
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:transparentBackground ? UIActivityIndicatorViewStyleGray : UIActivityIndicatorViewStyleWhiteLarge];
    activity.center = CGPointMake(view.center.x, view.center.y-40);
    activity.tag = kActivityIndicatorViewTag;
    [view addSubview:activity];
    [activity startAnimating];
    
    if (message) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, activity.frame.origin.y+activity.frame.size.height+8.0, view.frame.size.width-20.0, 40.0)];
        label.text = message;
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:20.0];
        label.textAlignment = NSTextAlignmentCenter;
        [view addSubview:label];
    } else {
        activity.center = CGPointMake(view.center.x, view.center.y);
    }
    
    [self.view addSubview:view];
    [UIView animateWithDuration:0.5 animations:^{
        view.alpha = 1.0;
    }];
}

- (void)removeLoadingView {
    [self removeLoadingViewWithCompletion:nil];
}

- (void)removeLoadingViewWithCompletion:(void (^)(void))completion {
    UIView *view = [self.view viewWithTag:kLoadingViewTag];
    [UIView animateWithDuration:0.5 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
        if (completion) {
            completion();
        }
    }];
}

- (BOOL)isShowingWaitingView {
    return [self.view viewWithTag:kLoadingViewTag] ? YES : NO;
}

@end

@implementation UIViewController (NibLoading)

+ (id)loadViewControllerFromNib {
    NSString *className = NSStringFromClass([self class]);
    id viewController = [[[self class] alloc] initWithNibName:className bundle:nil];
    return viewController;
}

@end

@implementation UIViewController (AddChildViewControler)

- (void)displayContentController:(UIViewController *)content {
  NSLog(@"%s", __FUNCTION__);
  [self addChildViewController:content];
  [content.view setFrame:[self.view bounds]];
  [self.view addSubview:content.view];
  [content didMoveToParentViewController:self];
}

- (void)hideContentController:(UIViewController *)content {
  [content willMoveToParentViewController:nil];
  [content.view removeFromSuperview];
  [content removeFromParentViewController];
}

- (void)simpleAlertControllerWithAction:(UIAlertAction *)action title:(NSString *)title message:(NSString *)message {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
  [alertController addAction:action];
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    [self presentViewController:alertController animated:YES completion:nil];
  }];
  
}


- (void)alertControllerWithActions:(NSArray *)actions title:(NSString *)title message:(NSString *)message {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
  for (UIAlertAction *action in actions) {
    [alertController addAction:action];
  }
  
  [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    [self presentViewController:alertController animated:YES completion:nil];
  }];
}

@end
