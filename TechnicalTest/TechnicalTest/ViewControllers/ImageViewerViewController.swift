//
//  ImageViewerViewController.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/28/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ImageViewerViewController: GeneralViewController {

  
  // MARK: - Properties
  var urlString: String?
  
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var imageView: UIImageView!
  
  
  // MARK: - Life view cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let _urlString = self.urlString, url = NSURL(string: _urlString) {
      imageView.af_setImageWithURL(url)
    }
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  

}
