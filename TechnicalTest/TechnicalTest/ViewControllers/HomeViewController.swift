//
//  HomeViewController.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/28/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//

import UIKit

class HomeViewController: GeneralViewController {

  // MARK: - Properties
  
  let pokemonFetcher = PokemonFetcher()
  var dataSource = PokemonDataSource()
  
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var tableView: UITableView!
  
  
  // MARK: - Life view cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.navigationBar.hidden = false
    configDataSource()
    configTableView()
    configNavigationBar()
    title = "Pokémon"
  }

  
  // MARK: - Configurations
  
  func configTableView() {
    tableView.dataSource  = dataSource
    tableView.delegate    = dataSource
  }
  
  func configDataSource() {
    dataSource.delegate = self
    dataSource.pokemonFetcher = pokemonFetcher
    dataSource.requestPokemons()
  }
  
  func configNavigationBar () {
    let barButton = UIBarButtonItem(title: "Logout", style: .Plain, target: self, action: #selector(HomeViewController.logout))
    navigationItem.leftBarButtonItem = barButton
    
    let infoButton = UIBarButtonItem(title: "Info", style: .Plain, target: self, action: #selector(HomeViewController.showInfoViewcontroller))
    navigationItem.rightBarButtonItem = infoButton
  }
  
  
  // MARK: - Actions
  
  func logout() {
    DataAccessor.sharedInstance.deleteInformation()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    appDelegate.didLogout()
  }
  
  func showInfoViewcontroller() {
    let identifier = String(InfoViewController)
    if let infoViewController = storyboard?.instantiateViewControllerWithIdentifier(identifier) {
      navigationController?.pushViewController(infoViewController, animated: true)
    }
    
  }
 
}


// MARK: - PokemonDataSourceDelegate

extension HomeViewController: PokemonDataSourceDelegate {
  func shouldUpdateTableView(sender: PokemonDataSource) {
    tableView.reloadData()
  }
  
  func shouldShowLoadingView(show: Bool) {
    if (show) {
      showLoadingViewWithMessage("Downloading info...")
    } else {
      removeLoadingView()
    }
  }
  
  func pokemonDataSource(dataSource: PokemonDataSource, didSelectPokemon pokemon: Pokemon) {
    let identifier = String(PokemonDetailViewController)
    if let pokemonDetailViewController = storyboard?.instantiateViewControllerWithIdentifier(identifier) as? PokemonDetailViewController {
      pokemonDetailViewController.pokemon = pokemon
      pokemonDetailViewController.title   = pokemon.name
      navigationController?.pushViewController(pokemonDetailViewController, animated: true)
    }
  }
  
  func pokemonDataSource(dataSource: PokemonDataSource, didSelectPhotos object: AnyObject?) {
    if let cell = object as? PokemonTableViewCell, indexPath = tableView.indexPathForCell(cell) {
      let pokemon   = dataSource.pokemons[indexPath.row]
      let identifier = String(ImageViewerViewController)
      if let imageViewerViewController = storyboard?.instantiateViewControllerWithIdentifier(identifier) as? ImageViewerViewController {
        imageViewerViewController.urlString = pokemon.imageURL
        imageViewerViewController.title     = pokemon.name
        navigationController?.pushViewController(imageViewerViewController, animated: true)
      }
    }
  }
}
