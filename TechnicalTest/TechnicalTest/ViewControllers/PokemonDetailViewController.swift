//
//  PokemonDetailViewController.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/28/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//

import UIKit

class PokemonDetailViewController: GeneralViewController {
  
  
  // MARK: - Properties
  
  var pokemon: Pokemon?
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var heightLabel: UILabel!
  @IBOutlet weak var weightLabel: UILabel!
  @IBOutlet weak var statsLabel: UILabel!
  @IBOutlet weak var textView: UITextView!
  
  // MARK: - Life view cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewDidLayoutSubviews() {
    configView()
  }
  
  
  // MARK: - Configurations
  func configView() {
    guard let pokemon = self.pokemon else {
      print("pokemon is nil")
      return
    }
    
    if let url = NSURL(string: pokemon.imageURL) {
      imageView.af_setImageWithURL(url)
    }
    heightLabel.text = "Height: \(pokemon.height)"
    weightLabel.text = "Weight: \(pokemon.weight)"
    
    var statsString = ""
    
    if let stats = pokemon.stats {
      for stat in stats {
        if let _stat = stat as? Stat {
          statsString += _stat.name + "\n"
        }
      }
    }
    
    textView.text = statsString
  }
  
  
}
