//
//  PokemonTableViewCell.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/28/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//

import UIKit

protocol PokemonTableViewCellDelegate {
  func cell(cell: PokemonTableViewCell, didSelectPhotos sender: AnyObject)
}

class PokemonTableViewCell: UITableViewCell {

  // MARK: - Properties
  
  var delegate: PokemonTableViewCellDelegate? 
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var photosButton: UIButton!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  // MARK: - Actions

  @IBAction func photosTouchUpInside(sender: AnyObject) {
    delegate?.cell(self, didSelectPhotos: sender)
  }
}
