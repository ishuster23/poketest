//
//  PokemonDataSource.swift
//  TechnicalTest
//
//  Created by Alejandro Cárdenas on 11/28/16.
//  Copyright © 2016 alekscbarragan. All rights reserved.
//

import UIKit

protocol PokemonDataSourceDelegate {
  func shouldUpdateTableView(sender: PokemonDataSource)
  func shouldShowLoadingView(show: Bool)
  func pokemonDataSource(dataSource: PokemonDataSource, didSelectPhotos object: AnyObject?)
  func pokemonDataSource(dataSource: PokemonDataSource, didSelectPokemon pokemon: Pokemon)
  
}

// MARK: - Data source class

class PokemonDataSource: NSObject {
  
  var delegate: PokemonDataSourceDelegate?
  var pokemonFetcher: PokemonFetcher!
  var pokemons = [Pokemon]()
  
  override init() {
    super.init()
  }
  
  
  func requestPokemons() {
    delegate?.shouldShowLoadingView(true)
    let pokemons = DataAccessor.sharedInstance.currentPokemons()
    
    if pokemons.count > 0 {
      self.pokemons = pokemons
      delegate?.shouldUpdateTableView(self)
      delegate?.shouldShowLoadingView(false)
    } else {
      pokemonFetcher.requestPokemons { (finished) in
        if (finished) {
          NSOperationQueue.mainQueue().addOperationWithBlock({ 
            DataAccessor.sharedInstance.saveContext()
          })
          self.pokemons = DataAccessor.sharedInstance.currentPokemons()
          self.delegate?.shouldUpdateTableView(self)
          self.delegate?.shouldShowLoadingView(false)
        }
      }
    }

  }
  
}


// MARK: - Pokemon cell delegate

extension PokemonDataSource: PokemonTableViewCellDelegate {
  func cell(cell: PokemonTableViewCell, didSelectPhotos sender: AnyObject) {
    delegate?.pokemonDataSource(self, didSelectPhotos: cell)
  }
}


// MARK: - table view data source

extension PokemonDataSource: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return pokemons.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let identifier = String(PokemonTableViewCell)
    let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! PokemonTableViewCell
    let pokemon = pokemons[indexPath.row]
    
    cell.nameLabel.text = pokemon.name
    cell.delegate = self
    
    return cell
  }
}


// MARK: - Table view delegate

extension PokemonDataSource: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let pokemon = pokemons[indexPath.row]
    delegate?.pokemonDataSource(self, didSelectPokemon: pokemon)
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
}
